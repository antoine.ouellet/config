# config - configuration and dotfiles
This repository hosts my configuration and dotfiles, along with python
 utilities to get my systems up-and-running to my standard preferences.

## Requirements
Utilities require Python 3 (only the standard library). 

## Installation
No particular installation is needed. The repository is cloned locally:
```git clone https://gitlab.com/antoine.ouellet/config.git```

Python scripts are not installed system-wise but run directly from the scripts
 folder
```cd ./scripts```
```python <script>.py [-<flags>]```

## Usage

### Pushing repository dotfiles to system
Configuration dotfiles from repository `./dotfiles` can be pushed to local
 system via the `dotfiles-push.py` script. I recommend the interactive `-i`
 option (for prompts before every file push), the back-up `-c` option (for
 saving a copy of pre-existing file to file.old) and the symlink option `-l`
 (for synching home dotfiles to repository dotfiles).
```python dotfiles-push.py -icl```

## Contributing
This project is not actively looking for contributors as it is intended for
 my personal use. You are however welcome to clone, fork and use

## Authors and acknowledgment
This repository was written by Antoine Ouellet.

## License
For open source projects, say how it is licensed.

## Project status
The project is still in development.
