import argparse
import os
import pathlib
import shutil


# Command-line arguments parsing
parser = argparse.ArgumentParser()

help_n = 'perform a dry run'
help_i = 'ask before copying dotfile'
help_l = 'dotfiles are replaced by links to repository dotfiles'
help_c = 'make back-ups of old versions'
help_v = 'list dotfiles to copy'
help_u = 'move to a different user home'

parser.add_argument('-n', '--dry_run', action='store_true', help=help_n)
parser.add_argument('-i', '--interactive', action='store_true', help=help_i)
parser.add_argument('-l', '--link', action='store_true', help=help_u)
parser.add_argument('-c', '--backup', action='store_true', help=help_c)
parser.add_argument('-v', '--verbose', action='store_true', help=help_v)
parser.add_argument('-u', '--user', action='store', help=help_u)

args = parser.parse_args()


def dotfile_push(src: str, dst: str, preserve=False, dst_link=False):
    """Push source dotfile to destination.
    """
    if preserve:
        shutil.copyfile(dst, f'{dst}.old')

    if dst_link:
        if os.path.exists(dst):
            os.remove(dst)
        os.symlink(os.path.abspath(src), dst)
    else:
        if os.path.exists(dst):
            os.remove(dst)
        shutil.copyfile(src, dst)


def main(n, i, li, c, v, u):
    """Iterate over all repository dotfiles and copy to home folder.
    """
    os.chdir(os.path.dirname(__file__))
    os.chdir('../dotfiles')

    if args.user:
        home = f'/home/{args.user}'
    else:
        home = pathlib.Path.home()

    # Check if header needs to be printed
    if args.verbose and not args.dry_run:
        print(f'Repository dotfiles will be written to {home}')
    if args.verbose and args.dry_run:
        print(f'*Dry run* Repository dotfiles would be written to {home}')

    if args.dry_run or args.verbose:
        string = 'REPOSITORY DOTFILE'
        print(f'\n{string: <40} DESTINATION')

    # Iterate over repository dotfiles
    for root, folders, files in os.walk('.'):
        for file in files:
            # Origin repository dotfile
            origin = os.path.join(root, file)

            # Destination home dotfile
            parts = pathlib.Path(root).parts[:]
            dest_list = [part for part in parts]
            destin = ''
            for dest in dest_list:
                destin = os.path.join(destin, dest)
            destination = os.path.join(home, destin)
            destination = os.path.join(destination, file)

            # Check if file names need to be printed
            if args.dry_run or args.verbose or args.interactive:
                print(f'{origin: <40} {destination}')

            # If the run is carried
            if not args.dry_run:
                # If carried interactively
                if args.interactive:
                    done = False
                    while not done:
                        ans = input(
                            '  do you want to copy(crush) this file? [y]/n: '
                        )
                        if ans == 'y' or ans == '':
                            dotfile_push(
                                origin,
                                destination,
                                preserve=args.backup,
                                dst_link=args.link
                            )
                            print('File copied.')
                            done = True
                        elif ans == 'n':
                            print('File not copied.')
                            done = True
                        else:
                            pass

                # If not carried interactively
                else:
                    dotfile_push(
                        origin,
                        destination,
                        preserve=args.backup,
                        dst_link=args.link
                    )

    if args.verbose and not args.dry_run:
        print('\nCopying dotfiles is completed.')

    if args.dry_run and args.interactive:
        print('  \nNothing was done yet.')
        rerun = input('  Do you want to perform that run? y/[n]: ')
        print()

        if rerun == 'y':
            args.dry_run = False
            args.interactive = False
            main(
                args.dry_run,
                args.interactive,
                args.link,
                args.backup,
                args.verbose,
                args.user
            )

        if rerun == 'n' or rerun == '':
            pass


if __name__ == '__main__':
    main(
        args.dry_run,
        args.interactive,
        args.link,
        args.backup,
        args.verbose,
        args.user
    )
